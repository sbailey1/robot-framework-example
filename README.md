## Test Automation: Python with Robot Framework - In Progress
[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/demo-gitlab-ci-robot-framework/badges/master/pipeline.svg)](https://gitlab.com/fxs/demo-gitlab-ci-robot-framework/pipelines)
[![Robot Framework Tests](https://fxs.gitlab.io/demo-gitlab-ci-robot-framework/all_tests.svg)](https://fxs.gitlab.io/demo-gitlab-ci-robot-framework/report.html)

![ani-robot](https://gitlab.com/tmedlin-demo/straight-outta-qa/-/raw/master/robot.gif)

# GitLab DevOps Stages

 1. MANAGE 🗝️
 2. SECURE 🔒
 3. PLAN 📝
 4. RELEASE 🏃
 5. PACKAGE 🎁
 6. DEVOPS 🏎️
 7. CREATE 🎨
 8. VERIFY ✅
 9. CONFIGURE ⚙️
 10. PROTECT 🛡️

An example of project to run [Robot Framework](https://robotframework.org/) tests in a [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) pipeline.

Example of test report is published with GitLab pages and is available [here](https://fxs.gitlab.io/demo-gitlab-ci-robot-framework/report.html).

## Run tests locally (debug)

In order to run tests locally you need docker and make.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

Example to run Robot Framework tests in your current directory:

```shell
# To show available commands:
make
# Example: to run Selenium tests with Chrome browser:
make tests-local browser=gc
```


## SmartyPants

SmartyPants converts ASCII punctuation characters into "smart" typographic punctuation HTML entities. For example:

|                |ASCII                          |HTML                         |
|----------------|-------------------------------|-----------------------------|
|Single backticks|`'Isn't this fun?'`            |'Isn't this fun?'            |
|Quotes          |`"Isn't this fun?"`            |"Isn't this fun?"            |
|Dashes          |`-- is en-dash, --- is em-dash`|-- is en-dash, --- is em-dash|


## KaTeX

You can render LaTeX mathematical expressions using [KaTeX](https://khan.github.io/KaTeX/):

The *Gamma function* satisfying $\Gamma(n) = (n-1)!\quad\forall n\in\mathbb N$ is via the Euler integral

$$
\Gamma(z) = \int_0^\infty t^{z-1}e^{-t}dt\,.
$$

> You can find more information about **LaTeX** mathematical expressions [here](http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference).


## UML diagrams

You can render UML diagrams using [Mermaid](https://mermaidjs.github.io/). For example, this will produce a sequence diagram:

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

And this will produce a flow chart:

```mermaid
graph LR
A[Square Rect] -- Link text --> B((Circle))
A --> C(Round Rect)
B --> D{Rhombus}
C --> D
